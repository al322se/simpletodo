﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GcpBlog.Application.Requests;
using GcpBlog.Domain;
using GcpBlog.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace GcpBlog.Application.Handlers.Command
{
    public class UpdatePost
    {
        public class Command : IRequest
        {
            public UpdatePostRequest Request { get; }

            public Command(UpdatePostRequest request)
            {
                Request = request;
            }
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly Context _context;
            
            public Handler(Context context)
            {
                _context = context;
            }

            protected  override async Task Handle(Command command, CancellationToken cancellationToken)
            {

                var findedPost= await _context.Posts.Where(x => x.Slug == command.Request.Slug).FirstOrDefaultAsync();
                if (findedPost == null)
                {
                    throw new Exception("There is not post whith that slug");
                }

                findedPost.Body = command.Request.Body;
                findedPost.Title = command.Request.Title;

                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}