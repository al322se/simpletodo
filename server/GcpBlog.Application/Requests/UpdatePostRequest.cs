﻿namespace GcpBlog.Application.Requests
{
    public class UpdatePostRequest
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Slug { get; set; }
    }
}